<?php

namespace BlackSmurf\Symfony2CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class BlackSmurfSymfony2CoreBundle extends Bundle {

    public function boot() {
        if (!$this->container->hasParameter('blacksmurfsymfony2core_bundle_pdf.dompdf')) {
            return;
        }

        $config = $this->container->getParameter('blacksmurfsymfony2core_bundle_pdf.dompdf');
        foreach ($config as $k => $v) {
            //$constKey = strtoupper($k);
            if (!defined($k)) {
                $value = $this->container->getParameterBag()->resolveValue($v);
                define($k, $value);
            }
        }
    }

    public function build(ContainerBuilder $container) {
        parent::build($container);
    }

}
