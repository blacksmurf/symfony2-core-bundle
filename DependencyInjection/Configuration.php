<?php

namespace BlackSmurf\Symfony2CoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface {

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder() {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('blacksmurfsymfony2core_bundle_pdf');
        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
                ->children()
                ->scalarNode('file')->defaultValue('%kernel.root_dir%/../vendor/blacksmurf/symfony2-core-bundle/BlackSmurf/Symfony2CoreBundle/Lib/dompdf/dompdf_config.inc.php')->end()
                ->scalarNode('class')->defaultValue('DOMPDF')->end()
                ->end()
        ;

        $this->addPDFConfig($rootNode);

        return $treeBuilder;
    }

    /**
     * Adds the core DOMPDF configuration
     *
     * @param $rootNode
     */
    protected function addPDFConfig(ArrayNodeDefinition $rootNode) {
        $rootNode
                ->children()
                ->arrayNode('dompdf')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('DOMPDF_DIR')->defaultValue("%kernel.root_dir%/../vendor/blacksmurf/symfony2-core-bundle/BlackSmurf/Symfony2CoreBundle/Lib/dompdf")->end()
                ->scalarNode('DOMPDF_INC_DIR')->defaultValue("%kernel.root_dir%/../vendor/blacksmurf/symfony2-core-bundle/BlackSmurf/Symfony2CoreBundle/Lib/dompdf/include")->end()
                ->scalarNode('DOMPDF_LIB_DIR')->defaultValue("%kernel.root_dir%/../vendor/blacksmurf/symfony2-core-bundle/BlackSmurf/Symfony2CoreBundle/Lib/dompdf/lib")->end()
                ->scalarNode('DOMPDF_TEMP_DIR')->defaultValue(sys_get_temp_dir())->end()
                ->scalarNode('DOMPDF_FONT_DIR')->defaultValue("%kernel.root_dir%/../vendor/blacksmurf/symfony2-core-bundle/BlackSmurf/Symfony2CoreBundle/Lib/dompdf/lib/fonts/")->end()
                ->scalarNode('DOMPDF_FONT_CACHE')->defaultValue("%kernel.root_dir%/../vendor/blacksmurf/symfony2-core-bundle/BlackSmurf/Symfony2CoreBundle/Lib/dompdf/lib/fonts/")->end()
                ->scalarNode('DOMPDF_CHROOT')->defaultValue("%kernel.root_dir%/../vendor/blacksmurf/symfony2-core-bundle/BlackSmurf/Symfony2CoreBundle/Lib/dompdf")->end()
                ->scalarNode('DOMPDF_LOG_OUTPUT_FILE')->defaultValue("%kernel.root_dir%/../vendor/blacksmurf/symfony2-core-bundle/BlackSmurf/Symfony2CoreBundle/Lib/dompdf/lib/fonts/log.htm")->end()
                ->scalarNode('DOMPDF_PDF_BACKEND')->defaultValue("CPDF")->end()
                ->scalarNode('DOMPDF_DEFAULT_MEDIA_TYPE')->defaultValue("screen")->end()
                ->scalarNode('DOMPDF_DEFAULT_PAPER_SIZE')->defaultValue("letter")->end()
                ->scalarNode('DOMPDF_DEFAULT_FONT')->defaultValue("serif")->end()
                ->scalarNode('DOMPDF_DPI')->defaultValue(96)->end()
                ->scalarNode('DOMPDF_ENABLE_PHP')->defaultValue(true)->end()
                ->scalarNode('DOMPDF_ENABLE_JAVASCRIPT')->defaultValue(true)->end()
                ->scalarNode('DOMPDF_ENABLE_REMOTE')->defaultValue(false)->end()
                ->scalarNode('DOMPDF_FONT_HEIGHT_RATIO')->defaultValue(1.1)->end()
                ->scalarNode('DOMPDF_ENABLE_CSS_FLOAT')->defaultValue(true)->end()
                ->scalarNode('DOMPDF_UNICODE_ENABLED')->defaultValue(true)->end()
                ->scalarNode('DOMPDF_ENABLE_FONTSUBSETTING')->defaultValue(false)->end()
                ->scalarNode('DOMPDF_ENABLE_AUTOLOAD')->defaultValue(true)->end()
                ->scalarNode('DOMPDF_AUTOLOAD_PREPEND')->defaultValue(false)->end()
                ->scalarNode('DOMPDF_ENABLE_HTML5PARSER')->defaultValue(false)->end()
                ->scalarNode('DEBUGPNG')->defaultValue(false)->end()
                ->scalarNode('DEBUGKEEPTEMP')->defaultValue(false)->end()
                ->scalarNode('DEBUGCSS')->defaultValue(false)->end()
                ->scalarNode('DEBUG_LAYOUT')->defaultValue(false)->end()
                ->scalarNode('DEBUG_LAYOUT_LINES')->defaultValue(true)->end()
                ->scalarNode('DEBUG_LAYOUT_BLOCKS')->defaultValue(true)->end()
                ->scalarNode('DEBUG_LAYOUT_INLINE')->defaultValue(true)->end()
                ->scalarNode('DEBUG_LAYOUT_PADDINGBOX')->defaultValue(true)->end()
                ->scalarNode('DOMPDF_ADMIN_USERNAME')->defaultValue("user")->end()
                ->scalarNode('DOMPDF_ADMIN_PASSWORD')->defaultValue("password")->end()
                ->end()
                ->end()
                ->end()
        ;
//                ->scalarNode('')->defaultValue("")->end()
    }

}
