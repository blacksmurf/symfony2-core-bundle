<?php

namespace BlackSmurf\Symfony2CoreBundle\Services;

use Symfony\Component\Templating\EngineInterface;

class MailService {

    protected $mailer;
    protected $templating;

    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating) {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }


    public function sendNewMessage($from, $to, $subject, $content) {
        $body = $this->templating->render("SmurfBusinessBundle:Bill:mail.html.twig", $content);

        $mail = \Swift_Message::newInstance();
        $mail->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->setBody($body)
            ->setContentType('text/html');


        $this->mailer->send($mail);
    }
}
