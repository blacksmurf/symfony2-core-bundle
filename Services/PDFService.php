<?php

namespace BlackSmurf\Symfony2CoreBundle\Services;

use ReflectionClass;

class PDFService {

    protected $className;

    public function __construct($className) {
        $this->setClassName($className);
    }

    public function create() {
        $rc = new ReflectionClass($this->className);
        return $rc->newInstanceArgs(func_get_args());
    }

    public function setClassName($className) {
        $rc = new ReflectionClass($className);
        if (!$rc->isSubclassOf('DOMPDF') && $rc->getName() != 'DOMPDF') {
            throw new \LogicException("Class '{$className}' must inherit from DOMPDF");
        }
        $this->className = $className;
    }

}
