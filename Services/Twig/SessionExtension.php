<?php

namespace BlackSmurf\Symfony2CoreBundle\Services\Twig;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\SecurityContextInterface;
use BlackSmurf\Symfony2CoreBundle\Services\Helper\UserGroupRoleSessionTokenHelper;

class SessionExtension extends \Twig_Extension {

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;

    /**
     *
     * @var \Symfony\Component\Security\Core\SecurityContextInterface;
     */
    private $securityContext;

    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;

    public function __construct(Container $container, SecurityContextInterface $securityContext, Registry $doctrine) {
        $this->container = $container;
        $this->securityContext = $securityContext;
        $this->doctrine = $doctrine;
    }

    public function getFunctions() {
        return array(
            'get_current_group' => new \Twig_Function_Method($this, 'getCurrentGroup', array('is_safe' => array('html'))),
        );
    }

    public function getCurrentGroup() {
        /* @var Symfony\Component\HttpFoundation\Session\Session */
        $session = $this->container->get("session");
        $token = $this->securityContext->getToken();
//        $selectedGroupId = $session->get($this->helper->getGroupKey($token));
        $selectedGroupId = UserGroupRoleSessionTokenHelper::get($session, $token);

        $em = $this->doctrine->getManager();
        $usergroup = $em->getRepository("BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole")->find($selectedGroupId);

        return $usergroup;
    }

    public function getName() {
        return 'session_extension';
    }

}
