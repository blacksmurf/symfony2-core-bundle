<?php

namespace BlackSmurf\Symfony2CoreBundle\Services\Twig;

use Symfony\Component\Routing\Router;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\SecurityContext;
use BlackSmurf\Symfony2CoreBundle\Services\Helper\MenuHelper;

class MenuExtension extends \Twig_Extension {

    protected $router;
    protected $securityContext;
    private $container;

    public function __construct(Router $router, Container $container, SecurityContext $securityContext) {
        $this->router = $router;
        $this->container = $container;
        $this->securityContext = $securityContext;
    }

    public function getFunctions() {
        return array(
            'render_menu' => new \Twig_Function_Method($this, 'renderMenu', array('is_safe' => array('html'))),
        );
    }

    public function renderMenu() {
        $session = $this->container->get("session");
        $token = $this->securityContext->getToken();
        $html = "";

        $html .= "<ul class=\"nav nav-pills nav-stacked\">";

        $items = unserialize(MenuHelper::get($session, $token));

        if (is_null($items) || count($items) <= 0) {
            return;
        }

        $request = null;
        if ($this->container->isScopeActive('request')) {
            $request = $this->container->get('request');
        }

        foreach ($items as $item) {
            $route = $item["route"];
            $title = $item["title"];
            $html .= "<li";

            if (!is_null($request)) {
                $pattern = explode("_", $item["route"]);
                if (preg_match("/^" . $pattern[0] . "\_/", $request->get("_route"))) {
                    $html .= " class=\"active\"";
                }
            }

            $html .= "><a href=\"" . $this->router->generate($route) . "\">$title</a></li>";
        }

        $html .= "</ul>";

        return $html;
    }

    public function getName() {
        return 'menu_extension';
    }

}
