<?php

namespace BlackSmurf\Symfony2CoreBundle\Services\Twig;

use Symfony\Component\Form\Extension\Csrf\CsrfProvider\SessionCsrfProvider;

class WidgetExtension extends \Twig_Extension {

    const PRIMARY = "primary";
    const DANGER = "danger";
    const WARNING = "warning";
    const INFO = "info";
    const SUCCESS = "success";
    // size
    const SIZE_LARGE = "lg";
    const SIZE_SMALL = "sm";
    const SIZE_EXTRA_SMALL = "xs";
    // icons
    const ICON_ASTERISK = "asterisk";
    const ICON_PLUS = "plus";
    const ICON_EURO = "euro";
    const ICON_MINUS = "minus";
    const ICON_CLOUD = "cloud";
    const ICON_ENVELOPE = "envelope";
    const ICON_PENCIL = "pencil";
    const ICON_GLASS = "glass";
    const ICON_MUSIC = "music";
    const ICON_SEARCH = "search";
    const ICON_HEART = "heart";
    const ICON_STAR = "star";
    const ICON_STAR_EMPTY = "star-empty";
    const ICON_USER = "user";
    const ICON_FILM = "film";
    const ICON_TH_LARGE = "th-large";
    const ICON_TH = "th";
    const ICON_TH_LIST = "th-list";
    const ICON_OK = "ok";
    const ICON_REMOVE = "remove";
    const ICON_ZOOM_IN = "zoom-in";
    const ICON_ZOOM_OUT = "zoom-out";
    const ICON_OFF = "off";
    const ICON_SIGNAL = "signal";
    const ICON_COG = "cog";
    const ICON_TRASH = "trash";
    const ICON_HOME = "home";
    const ICON_FILE = "file";
    const ICON_TIME = "time";
    const ICON_ROAD = "road";
    const ICON_DOWNLOAD_ALT = "download-alt";
    const ICON_DOWNLOAD = "download";
    const ICON_UPLOAD = "upload";
    const ICON_INBOX = "inbox";
    const ICON_PLAY_CIRCLE = "play-circle";
    const ICON_REFRESH = "refresh";
    const ICON_LIST_ALT = "list-alt";
    const ICON_LOCK = "lock";
    const ICON_FLAG = "flag";
    const ICON_HEADPHONES = "headphones";
    const ICON_VOLUME_OFF = "volume-off";
    const ICON_VOLUME_DOWN = "volume-down";
    const ICON_VOLUME_UP = "volume-up";
    const ICON_QRCODE = "qrcode";
    const ICON_BARCODE = "barcode";
    const ICON_TAG = "tag";
    const ICON_TAGS = "tags";
    const ICON_BOOK = "book";
    const ICON_BOOKMARK = "bookmark";
    const ICON_PRINT = "print";
    const ICON_CAMERA = "camera";
    const ICON_FONT = "font";
    const ICON_BOLD = "bold";
    const ICON_ITALIC = "italic";
    const ICON_TEXT_HEIGHT = "text-height";
    const ICON_TEXT_WIDTH = "text-width";
    const ICON_ALIGN_LEFT = "align-left";
    const ICON_ALIGN_CENTER = "align-center";
    const ICON_ALIGN_RIGHT = "align-right";
    const ICON_ALIGN_JUSTIFY = "align-justify";
    const ICON_LIST = "list";
    const ICON_IDENT_LEFT = "ident-left";
    const ICON_IDENT_RIGHT = "ident-right";
    const ICON_FACETIME_VIDEO = "facetime-video";
    const ICON_PICTURE = "picture";
    const ICON_MAP_MARKER = "map-marker";
    const ICON_ADJUST = "adjust";
    const ICON_TINT = "tint";
    const ICON_EDIT = "edit";
    const ICON_SHARE = "share";
    const ICON_CHECK = "check";
    const ICON_MOVE = "move";
    const ICON_STEP_BACKWARD = "step-backward";
    const ICON_FAST_BACKWARD = "fast-backward";
    const ICON_BACKWARD = "backward";
    const ICON_PLAY = "play";
    const ICON_PAUSE = "pause";
    const ICON_STOP = "stop";
    const ICON_FORWARD = "forward";
    const ICON_FAST_FORWARD = "fast-forward";
    const ICON_STEP_FORWARD = "step-forward";
    const ICON_EJECT = "eject";
    const ICON_CHEVRON_LEFT = "chevron-left";
    const ICON_CHEVRON_RIGHT = "chevron-rigth";
    const ICON_PLUS_SIGN = "plus-sign";
    const ICON_MINUS_SIGN = "minus-sign";
    const ICON_REMOVE_SIGN = "remove-sign";
    const ICON_OK_SIGN = "ok-sign";
    const ICON_QUESTION_SIGN = "question-sign";
    const ICON_INFO_SIGN = "info-sign";
    const ICON_SCREENSHOT = "screenshot";
    const ICON_REMOVE_CIRCLE = "remove-circle";
    const ICON_OK_CIRCLE = "ok-circle";
    const ICON_BAN_CIRCLE = "ban-circle";
    const ICON_ARROW_LEFT = "arrow-left";
    const ICON_ARROW_RIGHT = "arrow-right";
    const ICON_ARROW_UP = "arrow-up";
    const ICON_ARROW_DOWN = "arrow-down";
    const ICON_SHARE_ALT = "share-alt";
    const ICON_RESIZE_FULL = "resize-full";
    const ICON_RESIZE_SMALL = "resize-small";
    const ICON_EXCLAMATION_SIGN = "exclamation-sign";
    const ICON_GIFT = "gift";
    const ICON_LEAF = "leaf";
    const ICON_FIRE = "fire";
    const ICON_EYE_OPEN = "eye-open";
    const ICON_EYE_CLOSE = "eye-close";
    const ICON_WARNING_SIGN = "warning-sign";
    const ICON_PLANE = "plane";
    const ICON_CALENDAR = "calendar";
    const ICON_RANDOM = "random";
    const ICON_COMMENT = "comment";
    const ICON_MAGNET = "magnet";
    const ICON_CHEVRON_UP = "chevron-up";
    const ICON_CHEVRON_DOWN = "chevron-down";
    const ICON_RETWEET = "retweet";
    const ICON_SHOPPING_CART = "shopping-cart";
    const ICON_FOLDER_CLOSE = "folder-close";
    const ICON_FOLDER_OPEN = "folder-open";
    const ICON_RESIZE_VERTICAL = "resize-vertical";
    const ICON_RESIZE_HORIZONTAL = "resize-horizontal";
    const ICON_HDD = "hdd";
    const ICON_BULLHORN = "bullhorn";
    const ICON_BELL = "bell";
    const ICON_CERTIFICATE = "certificate";
    const ICON_THUMBS_UP = "thumbs-up";
    const ICON_THUMBS_DOWN = "thumbs-down";
    const ICON_HAND_RIGHT = "hand-right";
    const ICON_HAND_LEFT = "hand-left";
    const ICON_HAND_UP = "hand-up";
    const ICON_HAND_DOWN = "hand-down";
    const ICON_CIRCLE_ARROW_RIGHT = "circle-arrow-right";
    const ICON_CIRCLE_ARROW_LEFT = "circle-arrow-left";
    const ICON_CIRCLE_ARROW_UP = "circle-arrow-up";
    const ICON_CIRCLE_ARROW_DOWN = "circle-arrow-down";
    const ICON_GLOBE = "globe";
    const ICON_WRENCH = "wrench";
    const ICON_TASKS = "tasks";
    const ICON_FILTER = "filter";
    const ICON_BRIEFCASE = "briefcase";
    const ICON_FULLSCREEN = "fullscreen";
    const ICON_DASHBOARD = "dashboard";
    const ICON_PAPERCLIP = "paperclip";
    const ICON_HEART_EMPTY = "heart-empty";
    const ICON_LINK = "link";
    const ICON_PHONE = "phone";
    const ICON_PUSHPIN = "pushpin";
    const ICON_USD = "usd";
    const ICON_GBP = "gbp";
    const ICON_SORT = "sort";
    const ICON_SORT_BY_ALPHABET = "sort-by-alphabet";
    const ICON_SORT_BY_ALPHABET_ALT = "sort-by-alphabet-alt";
    const ICON_SORT_BY_ORDER = "sort-by-order";
    const ICON_SORT_BY_ORDER_ALT = "sort-by-order-alt";
    const ICON_SORT_BY_ATTRIBUTES = "sort-by-attributes";
    const ICON_SORT_BY_ATTRIBUTES_ALT = "sort-by-attributes-alt";
    const ICON_UNCHECKED = "unchecked";
    const ICON_EXPAND = "expand";
    const ICON_COLLAPSE_DOWN = "collapse-down";
    const ICON_COLLAPSE_UP = "collapse-up";
    const ICON_LOG_IN = "log-in";
    const ICON_FLASH = "flash";
    const ICON_LOG_OUT = "log-out";
    const ICON_NEW_WINDOW = "new-window";
    const ICON_RECORD = "record";
    const ICON_SAVE = "save";
    const ICON_OPEN = "open";
    const ICON_SAVED = "saved";
    const ICON_IMPORT = "import";
    const ICON_EXPORT = "export";
    const ICON_SEND = "send";
    const ICON_FLOPPY_DISK = "floppy-disk";
    const ICON_FLOPPY_SAVED = "floppy-saved";
    const ICON_FLOPPY_REMOVE = "floppy-remove";
    const ICON_FLOPPY_SAVE = "floppy-save";
    const ICON_FLOPPY_OPEN = "floppy-open";
    const ICON_CREDIT_CARD = "credit-card";
    const ICON_TRANSFER = "transfer";
    const ICON_CUTLERY = "cutlery";
    const ICON_HEADER = "header";
    const ICON_COMPRESSED = "compressed";
    const ICON_EARPHONE = "earphone";
    const ICON_PHONE_ALT = "phone-alt";
    const ICON_TOWER = "tower";
    const ICON_STATS = "stats";
    const ICON_SD_VIDEO = "sd-video";
    const ICON_HD_VIDEO = "hd-video";
    const ICON_SUBTITLES = "subtitles";
    const ICON_SOUND_STEREO = "sound-stereo";
    const ICON_SOUND_DOLBY = "sound-dolby";
    const ICON_SOUND_5_1 = "sound-5-1";
    const ICON_SOUND_6_1 = "sound-6-1";
    const ICON_SOUND_7_1 = "sound-7-1";
    const ICON_COPYRIGHT_MARK = "copyright-mark";
    const ICON_REGISTRATION_MARK = "registration-mark";
    const ICON_CLOUD_DOWNLOAD = "cloud-download";
    const ICON_CLOUD_UPLOAD = "cloud-upload";
    const ICON_TREE_CONIFER = "tree-conifer";
    const ICON_TREE_DECIDUOUS = "tree-deciduous";

    protected $csrfProvider;

    public function __construct(SessionCsrfProvider $csrfProvider) {
        $this->csrfProvider = $csrfProvider;
    }

    /**
     * Get class widget's constant!
     *
     * @return Object
     */
    public function getGlobals() {
        $class = new \ReflectionClass('BlackSmurf\Symfony2CoreBundle\Services\Twig\WidgetExtension');
        $constants = $class->getConstants();

        return array(
            'WidgetExtension' => $constants
        );
    }

    public function getFunctions() {
        return array(
            'get_link' => new \Twig_Function_Method($this, 'linkTo', array('is_safe' => array('html'))),
            'get_button' => new \Twig_Function_Method($this, 'getButton', array('is_safe' => array('html'))),
            'get_form_submit_button' => new \Twig_Function_Method($this, 'getFormSubmitButton', array('is_safe' => array('html'))),
            'get_crud_submit_button' => new \Twig_Function_Method($this, 'getCrudSubmitButton', array('is_safe' => array('html'))),
            'get_previous_button' => new \Twig_Function_Method($this, 'getPreviousButton', array('is_safe' => array('html'))),
        );
    }

    /**
     *
     * @param string $label
     * @param array $options Available options:
     *  string 'icon' - class of boostrap icon
     *  string 'type' - WidgetExtension::PRIMARY, WidgetExtension::INFO, WidgetExtension::SUCCESS, WidgetExtension::WARNING, WidgetExtension::DANGER
     *  string 'url' - onclick's event
     *  string 'id' - ID's element
     * @return type
     */
    public function getButton($label, array $options = array()) {
        $defaultParams = array(
            "type" => "default",
            "icon" => "",
            "url" => "",
            "id" => "",
            "size" => "",
            "class" => "",
        );

        $options = array_merge($defaultParams, $options);
        $id = empty($options["id"]) ? "" : sprintf("id=\"%s\" ", $options["id"]);
        $icon = empty($options["icon"]) ? "" : sprintf("<span class=\"glyphicon glyphicon-%s\"></span>&nbsp;", $options["icon"]);
        $type = sprintf("btn-%s ", $options["type"]);
        $size = empty($options["size"]) ? "" : sprintf("btn-%s ", $options["size"]);

        return sprintf("<button type=\"button\" %sclass=\"btn %s%s %s\" onclick=\"%s\">%s%s</button>", $id, $type, $size, $options["class"], $options['url'], $icon, $label);
    }

    /**
     *
     * @param string $label
     * @param array $options Available options:
     *  string 'icon' - class of boostrap icon
     *  string 'type' - WidgetExtension::PRIMARY, WidgetExtension::INFO, WidgetExtension::SUCCESS, WidgetExtension::WARNING, WidgetExtension::DANGER
     *  string 'url' - onclick's event
     *  string 'id' - ID's element
     * @return type
     */
    public function getFormSubmitButton($label, array $options = array()) {
        $defaultParams = array(
            "type" => WidgetExtension::PRIMARY,
            "icon" => "",
            "id" => "",
        );

        $options = array_merge($defaultParams, $options);
        $id = empty($options["id"]) ? "" : sprintf("id=\"%s\" ", $options["id"]);
        $icon = empty($options["icon"]) ? "" : sprintf("<span class=\"glyphicon glyphicon-%s\"></span>&nbsp;", $options["icon"]);
        $type = sprintf("btn-%s", $options["type"]);
        return sprintf("<button type=\"submit\" %sclass=\"btn %s\">%s%s</button>", $id, $type, $icon, $label);
    }

    /**
     * Build a Submit button for a CRUD form
     *
     * @param string $label
     * @param string $url
     * @return string
     */
    public function getCrudSubmitButton($label) {
        return $this->getButton($label, array(
                    "type" => WidgetExtension::DANGER,
                    "icon" => WidgetExtension::ICON_FLOPPY_SAVED,
                    "url" => "javascript:doCreate();",
                    "id" => "form_submit"
        ));
    }

    /**
     * Build a Previous button for a CRUD form
     *
     * @param string $label
     * @param string $url
     * @return string
     */
    public function getPreviousButton($label, $url) {
        return $this->getButton($label, array(
                    "type" => WidgetExtension::INFO,
                    "icon" => WidgetExtension::ICON_CHEVRON_LEFT,
                    "url" => sprintf("javascript:document.location.href = '%s'", $url),
        ));
    }

    /**
     * Build a link with anchor
     *
     * @param string $label
     * @param array $options Available options:
     *  string 'confirm' - Text for the popup
     *  string 'method' - HTTP Method: post, delete, put
     *  string 'csrf"' - CSRF
     * @return string
     */
    public function linkTo($label, array $options = array()) {
        $defaultParams = array(
            "url" => "",
            "method" => "",
            "confirm" => "",
            "icon" => "",
            "title" => "Link",
            "class" => "",
            "id" => "",
            "csrf" => false,
        );
        $options = array_merge($defaultParams, $options);

        $crudForm = "#crudActionForm";

        switch (strtolower($options["method"])) {
            case "post":
            case "put":
            case "delete":
                $method = "POST";
                $verb = strtoupper($options["method"]);
                break;
            default:
                $method = "GET";
                $verb = "";
        }

        $csrf = "";
        if ($options["csrf"]) {
            $csrf = sprintf("$('$crudForm #form__token').attr('value', '%s');", $this->csrfProvider->generateCsrfToken($options["url"]));
        }

        $link = sprintf("$('$crudForm').attr('method', '%s'); $('$crudForm input[name=\'_method\']').attr('value', '%s'); %s $('$crudForm').attr('action', '%s'); $('$crudForm').submit();", $method, $verb, $csrf, htmlspecialchars($options["url"]));

        $onclick = empty($options["confirm"]) ? sprintf("%s return false;", $link) : sprintf("if (confirm('%s')) { %s } return false;", htmlspecialchars($options["confirm"]), $link);

        $icon = empty($options["icon"]) ? "" : sprintf("<span class=\"glyphicon glyphicon-%s\"></span>&nbsp;", $options["icon"]);

        return sprintf("<a href=\"#\" onclick=\"%s\" class=\"%s\">%s%s</a>", $onclick, $options["class"], $icon, htmlspecialchars($label));
    }

    public function getName() {
        return 'linkto_extension';
    }

}
