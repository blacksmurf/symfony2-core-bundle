<?php

namespace BlackSmurf\Symfony2CoreBundle\Services\Events;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use BlackSmurf\Symfony2CoreBundle\Entity\User;
use BlackSmurf\Symfony2CoreBundle\Services\Helper\UserGroupRoleSessionTokenHelper;

class InteractiveLoginListener {

    /**
     * Listen for successful login events
     * @param \Symfony\Component\Security\Http\Event\InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event) {
        if (!$event->getAuthenticationToken() instanceof UsernamePasswordToken) {

            return;
        }

        //Check if user can select group authentication
        $token = $event->getAuthenticationToken();
        $session = $event->getRequest()->getSession();

        $user = $token->getUser();
        if (!$user instanceof User) {

            return;
        }

        // set flag in the session
        UserGroupRoleSessionTokenHelper::set($session, $token, null);
    }

}
