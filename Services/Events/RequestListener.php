<?php

namespace BlackSmurf\Symfony2CoreBundle\Services\Events;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use BlackSmurf\Symfony2CoreBundle\Services\Helper\MenuHelper;
use BlackSmurf\Symfony2CoreBundle\Services\Helper\UserGroupRoleSessionTokenHelper;

#use BlackSmurf\Symfony2CoreBundle\Services\Helper\SessionHelper;
#use BlackSmurf\Symfony2CoreBundle\Services\Helper\TokenHelper;

class RequestListener {

    /**
     * @var \Symfony\Component\Security\Core\SecurityContextInterface  $securityContext
     */
    protected $securityContext;
    protected $rootDir;
    protected $templating;
    protected $doctrine;

    /**
     * Construct the listener
     * @param \Symfony\Component\Security\Core\SecurityContextInterface $securityContext
     * @param \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface $templating
     */
    public function __construct($rootDir, SecurityContextInterface $securityContext, EngineInterface $templating, Registry $doctrine) {

        $this->rootDir = $rootDir;
        $this->securityContext = $securityContext;
        $this->templating = $templating;
        $this->doctrine = $doctrine;
    }

    public function buildMenu($session, $token) {
        $items = array();

        $configDirectories = array($this->rootDir . '/config');
        $locator = new FileLocator($configDirectories);

        $ymlFile = $locator->locate('menu.yml', null, false);

        if (!is_null($ymlFile)) {

            $yaml = Yaml::parse(file_get_contents($ymlFile[0]));

            foreach ($yaml["menu"] as $item) {
                $roles = explode(",", $item["roles"]);
                foreach ($roles as $key => $value) {

                    $value = trim($value);
                    $roles[$key] = $value;
                }
                if ($this->securityContext->isGranted($roles)) {
                    $items[] = $item;
                }
            }

            MenuHelper::set($session, $token, serialize($items));
        }
    }

    public function updateToken($token, $userGroupRoleId) {
        $user = $token->getUser();
        $em = $this->doctrine->getManager();
        $usergrouprole = $em->getRepository("BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole")->find($userGroupRoleId);
        $user->addRole($usergrouprole->getGroupRole()->getRole());
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->securityContext->setToken($token);
    }

    public function selectGroupRoleAndNext($event, $session, $token, $request, $selectedUserGroupRoleId) {
        UserGroupRoleSessionTokenHelper::set($session, $token, $selectedUserGroupRoleId);

        $this->updateToken($token, $selectedUserGroupRoleId);
        $this->buildMenu($session, $token);

        $baseurl = $request->getBaseUrl();
        $url = empty($baseurl) ? "/" : $baseurl;
        $redirect = new RedirectResponse($url);
        $event->setResponse($redirect);
    }

    /**
     * Listen for request events
     * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
     */
    public function onCoreRequest(GetResponseEvent $event) {
        $token = $this->securityContext->getToken();

        // si il y a un token
        if (!$token) {

            return;
        }

        /*
          if (TokenHelper::has($session, $token)) {
          $token = unserialize(TokenHelper::get($session, $token));
          $this->securityContext->setToken($token);
          }
         */

        // si l'utilisateur n'est pas loggé
        if (!$token instanceof UsernamePasswordToken) {

            return;
        }

        // sinon si l'utilisateur est loggé :
        $user = $token->getUser();
        $request = $event->getRequest();
        $session = $request->getSession();

        // si l'utilisateur n'a pas de groupe en session on sort
        if (!UserGroupRoleSessionTokenHelper::has($session, $token)) {

            return;
        }

        $userGroupRoleId = UserGroupRoleSessionTokenHelper::get($session, $token);

        // si l'utilisateur est loggé avec un groupe de sélectionné
        if ($userGroupRoleId != 0 && $userGroupRoleId != NULL) {
            $this->updateToken($token, $userGroupRoleId);

            return;
        }

        // si en session aucun groupe n'a été sélectionné, mais que la session avec le groupe est initialisé (demande de login)
        $groups = $user->getGroups();

        // si l'utilisateur possède plusieurs groupes
        if (count($groups) > 1) {

            // si method=POST, alors l'utilisateur vient de valider un formulaire de sélection du groupe
            if ($request->getMethod() == 'POST') {

                // check the group code
                $selectedUserGroupRoleId = $request->get('_auth_group_id');
                if ($user->hasGroup($selectedUserGroupRoleId) == true) {
                    return $this->selectGroupRoleAndNext($event, $session, $token, $request, $selectedUserGroupRoleId);
                } else {

                    $session->getFlashBag()->set("error", "The verification code is not valid.");
                }
            }

            // sinon on affiche le formulaire de sélection du groupe
            UserGroupRoleSessionTokenHelper::set($session, $token, 0);
            $response = $this->templating->renderResponse('BlackSmurfSymfony2CoreBundle:Security:listgroup.html.twig', array('groups' => $groups));
            $event->setResponse($response);
        } else {

            // si l'utilisateur ne possède qu'un groupe, alors sélection par défaut
            return $this->selectGroupRoleAndNext($event, $session, $token, $request, $groups[0]->getId());
        }
    }

}
