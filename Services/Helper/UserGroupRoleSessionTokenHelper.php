<?php

namespace BlackSmurf\Symfony2CoreBundle\Services\Helper;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class UserGroupRoleSessionTokenHelper extends SessionTokenHelper {

    /**
     *
     * @param TokenInterface $token
     * @return string
     */
    public static function getKeyName(TokenInterface $token) {
        return sprintf('blacksmurfsymfony2corebundle_usergrouprole_%s_%s', $token->getProviderKey(), $token->getUsername());
    }

}
