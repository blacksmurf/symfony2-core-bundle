<?php

namespace BlackSmurf\Symfony2CoreBundle\Services\Helper;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class SessionTokenHelper {

    /**
     *
     * @param TokenInterface $token
     * @return string
     */
    public static function getKeyName(TokenInterface $token) {
        return sprintf('blacksmurfsymfony2corebundle_session_%s_%s', $token->getProviderKey(), $token->getUsername());
    }

    /**
     *
     * @param Session $session
     * @param TokenInterface $token
     * @return bool
     */
    public final static function has(Session $session, TokenInterface $token) {
        return $session->has(static::getKeyName($token));
    }

    /**
     *
     * @param Session $session
     * @param TokenInterface $token
     * @return string
     */
    public final static function get(Session $session, TokenInterface $token) {
        return $session->get(static::getKeyName($token));
    }

    /**
     *
     * @param Session $session
     * @param TokenInterface $token
     * @param string $value
     * @return type
     */
    public final static function set(Session $session, TokenInterface $token, $value) {
        return $session->set(static::getKeyName($token), $value);
    }

}
