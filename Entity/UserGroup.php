<?php

namespace BlackSmurf\Symfony2CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserGroup
 */
class UserGroup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BlackSmurf\Symfony2CoreBundle\Entity\User
     */
    private $user;

    /**
     * @var \BlackSmurf\Symfony2CoreBundle\Entity\GroupRole
     */
    private $groupRole;

    /**
     * @var \BlackSmurf\BusinessBundle\Entity\Company
     */
    private $company;

    /**
     * @var \BlackSmurf\BusinessBundle\Entity\Client
     */
    private $client;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\User $user
     * @return UserGroup
     */
    public function setUser(\BlackSmurf\Symfony2CoreBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BlackSmurf\Symfony2CoreBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set groupRole
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\GroupRole $groupRole
     * @return UserGroup
     */
    public function setGroupRole(\BlackSmurf\Symfony2CoreBundle\Entity\GroupRole $groupRole)
    {
        $this->groupRole = $groupRole;

        return $this;
    }

    /**
     * Get groupRole
     *
     * @return \BlackSmurf\Symfony2CoreBundle\Entity\GroupRole
     */
    public function getGroupRole()
    {
        return $this->groupRole;
    }

    /**
     * Set company
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Company $company
     * @return UserGroup
     */
    public function setCompany(\BlackSmurf\BusinessBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set client
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Client $client
     * @return UserGroup
     */
    public function setClient(\BlackSmurf\BusinessBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
