<?php

namespace BlackSmurf\Symfony2CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="BlackSmurf\Symfony2CoreBundle\Entity\UserRepository")
 */
class User implements AdvancedUserInterface, \Serializable {

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=100)
     * @var string
     */
    protected $login;

    /**
     * @ORM\Column(type="string", length=25)
     * @var string
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=40)
     * @var string
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @var string
     */
    protected $salt;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    protected $activate;

    /**
     * @ORM\OneToMany(targetEntity="BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole", mappedBy="user")
     */
    protected $groups;

    /**
     *
     * @var Array()
     */
    private $roles;

    ////////////////////////////////////////////////////////////////////////////
    // MODIFIED CODE ///////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public function __construct() {
        $this->activate();
//                ->setSalt(md5(uniqid(null, true)));

        $this->groups = new ArrayCollection();
        $this->roles = new ArrayCollection();
    }

    public function getId() {
        return $this->getLogin();
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password) {
        $this->password = sha1($this->getSalt() . $password);
        return $this;
    }

    /**
     * Test password
     *
     * @param string $password
     * @return boolean
     */
    public function testPassword($password) {
        return sha1($password) == $this->password;
    }

    /**
     *
     * @return \BlackSmurf\Symfony2CoreBundle\Entity\User
     */
    public function activate() {
        $this->setActivate(true);
        return $this;
    }

    /**
     *
     * @return \BlackSmurf\Symfony2CoreBundle\Entity\User
     */
    public function desactivate() {
        $this->setActivate(false);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRoles() {
        /*
          $roles = $this->roles;
          $roles[] = "ROLE_USER";

          foreach ($this->groups as $group) {
          $roles[] = $group->getUserLevel()->getRole();
          }

          $this->roles = array_unique($roles);

          return array_unique($this->roles);
         */
        if (is_null($this->roles)) {
            $this->roles = array();
        }

        return array_merge(array("ROLE_USER"), $this->roles);
    }

    public function addRole($role) {
        $this->roles[] = $role;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials() {

    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize() {
        $login = $this->getLogin();
        return serialize(array(
            $login,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized) {
        list (
                $this->login,
                ) = unserialize($serialized);
    }

    public function hasGroup($refGroup) {
        foreach ($this->getGroups() as $group) {
            if ($group->getId() == $refGroup) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\User $user
     * @return boolean
     */
    public function isEqualTo(User $user) {
        return $this->getLogin() === $user->getLogin();
    }

    public function isAccountNonExpired() {
        return true;
    }

    public function isAccountNonLocked() {
        return true;
    }

    public function isCredentialsNonExpired() {
        return true;
    }

    public function isEnabled() {
        return $this->activate;
    }

    public function getSalt() {
        return $this->salt;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    ////////////////////////////////////////////////////////////////////////////
    // CODE ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Set login
     *
     * @param string $login
     * @return User
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string 
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set activate
     *
     * @param boolean $activate
     * @return User
     */
    public function setActivate($activate)
    {
        $this->activate = $activate;

        return $this;
    }

    /**
     * Get activate
     *
     * @return boolean 
     */
    public function getActivate()
    {
        return $this->activate;
    }

    /**
     * Add groups
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $groups
     * @return User
     */
    public function addGroup(\BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $groups)
    {
        $this->groups[] = $groups;

        return $this;
    }

    /**
     * Remove groups
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $groups
     */
    public function removeGroup(\BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $groups)
    {
        $this->groups->removeElement($groups);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroups()
    {
        return $this->groups;
    }
}
