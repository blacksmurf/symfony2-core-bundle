<?php

namespace BlackSmurf\Symfony2CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="grouprole")
 */
class GroupRole implements RoleInterface {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var Integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @var String
     */
    protected $name;

    /**
     * @ORM\Column(name="role", type="string", length=20, unique=true)
     */
    protected $role;

    /**
     * @ORM\OneToMany(targetEntity="BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole", mappedBy="groupRoleId")
     */
    protected $users;

    ////////////////////////////////////////////////////////////////////////////
    // MODIFIED CODE ///////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    public function __construct() {
        $this->users = new ArrayCollection();
    }

    /**
     * @see RoleInterface
     */
    public function getRole() {
        return $this->role;
    }

    ////////////////////////////////////////////////////////////////////////////
    // CODE ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GroupRole
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return GroupRole
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Add users
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $users
     * @return GroupRole
     */
    public function addUser(\BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $users
     */
    public function removeUser(\BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
}
