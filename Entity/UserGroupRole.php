<?php

namespace BlackSmurf\Symfony2CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="usergrouprole", uniqueConstraints={@ORM\UniqueConstraint(name="IDX_ONCE_RIGHT", columns={"userLogin", "groupRoleId", "companySiret", "clientId"})})
 */
class UserGroupRole {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="BlackSmurf\Symfony2CoreBundle\Entity\User", inversedBy="groups")
     * @ORM\JoinColumn(name="userLogin", referencedColumnName="login", nullable=false)
     * @var User
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="BlackSmurf\Symfony2CoreBundle\Entity\GroupRole", inversedBy="users")
     * @ORM\JoinColumn(name="groupRoleId", referencedColumnName="id", nullable=false)
     * @var User
     */
    protected $groupRole;

    /**
     * @ORM\ManyToOne(targetEntity="BlackSmurf\BusinessBundle\Entity\Company", inversedBy="rights")
     * @ORM\JoinColumn(name="companySiret", referencedColumnName="siret", nullable=true)
     * @var User
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="BlackSmurf\BusinessBundle\Entity\Client", inversedBy="rights")
     * @ORM\JoinColumn(name="clientId", referencedColumnName="id", nullable=true)
     * @var User
     */
    protected $client;

    ////////////////////////////////////////////////////////////////////////////
    // CODE ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\User $user
     * @return UserGroupRole
     */
    public function setUser(\BlackSmurf\Symfony2CoreBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BlackSmurf\Symfony2CoreBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set groupRole
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\GroupRole $groupRole
     * @return UserGroupRole
     */
    public function setGroupRole(\BlackSmurf\Symfony2CoreBundle\Entity\GroupRole $groupRole)
    {
        $this->groupRole = $groupRole;

        return $this;
    }

    /**
     * Get groupRole
     *
     * @return \BlackSmurf\Symfony2CoreBundle\Entity\GroupRole
     */
    public function getGroupRole()
    {
        return $this->groupRole;
    }

    /**
     * Set company
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Company $company
     * @return UserGroupRole
     */
    public function setCompany(\BlackSmurf\BusinessBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set client
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Client $client
     * @return UserGroupRole
     */
    public function setClient(\BlackSmurf\BusinessBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
