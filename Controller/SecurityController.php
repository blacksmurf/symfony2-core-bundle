<?php

namespace BlackSmurf\Symfony2CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Security controller.
 *
 * @Route("/_security")
 */
class SecurityController extends Controller {

    /**
     *
     * @Route("/login", name="blacksmurf_symfony2core_security_login")
     * @Method("GET")
     * @Template()
     */
    public function loginAction() {

        $request = $this->getRequest();
        $session = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        return $this->render('BlackSmurfSymfony2CoreBundle:Security:login.html.twig', array(
                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error' => $error
        ));
    }

    /**
     *
     * @Route("/login", name="blacksmurf_symfony2core_security_select_group")
     * @Method("POST")
     * @Template()
     * @Secure(roles="ROLE_USER")
     */
    public function dummyGroupAction() {
        return new Response();
    }

}
