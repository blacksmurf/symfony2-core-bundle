<?php

namespace BlackSmurf\Symfony2CoreBundle\Controller;

// SYMFONY //
use Symfony\Component\HttpFoundation\Request;
// ANNOTATIONS //
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

// PROJECT //
use BlackSmurf\Symfony2CoreBundle\Entity\UserRole;
use BlackSmurf\Symfony2CoreBundle\Form\UserType;

/**
 * Client controller.
 *
 * @Route("/user")
 */
class UserController extends EntityController {

    ////////////////////////////////////////////////////////////////////////////
    // MUST BE DEFINED /////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    protected $routes = array(
        "index" => "user_index",
        "new" => "user_new",
        "create" => "user_create",
        "edit" => "user_edit",
        "update" => "user_update",
        "delete" => "user_delete"
    );

    /**
     * Return string object URI
     *
     * @return string
     */
    protected function getEntityURI() {
        return "BlackSmurfBusinessBundle:User";
    }

    /**
     * Return new object
     *
     * @return User
     */
    protected function getNewEntity() {
        return new User();
    }

    /**
     * Return Form's object
     *
     * @return UserType
     */
    protected function getNewEntityFormType() {
        return new UserType();
    }

    ////////////////////////////////////////////////////////////////////////////
    // LISTING /////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Lists all User entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction() {

    }
}