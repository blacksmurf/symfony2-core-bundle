<?php

namespace BlackSmurf\Symfony2CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BlackSmurf\Symfony2CoreBundle\Services\Helper\UserGroupRoleSessionTokenHelper;

abstract class EntityController extends Controller {

    protected $routes = null;

    abstract protected function getEntityURI();

    abstract protected function getNewEntity();

    abstract protected function getNewEntityFormType();

    protected function getRoute($index) {
        if (count($this->routes) > 0) {
            if (array_key_exists($index, $this->routes)) {
                return $this->routes[$index];
            }
        }

        throw new \Exception("Vous devez spécifier une route !");
    }

    protected function buildParameters($options) {
        $defaultOptions = array(
            'page_title' => 'Titre de la page',
            'back_url' => $this->getRoute('index'),
        );

        $options = array_merge($defaultOptions, $options);

        return $options;
    }

    protected function createMyForm($entity, $action, $method) {
        $form = $this->createForm($this->getNewEntityFormType(), $entity, array(
            'action' => $action,
            'method' => $method,
        ));

        $form->add('submit', 'submit', array('label' => 'Valider'));

        return $form;
    }

    protected function errorMessage($message) {
        $this->get('session')->getFlashBag()->add('error', $message);
    }

    protected function successMessage($message) {
        $this->get('session')->getFlashBag()->add('notice', $message);
    }

    protected function getMyUserGroupRoleId() {
        return UserGroupRoleSessionTokenHelper::get($this->getMySession(), $this->getMyToken());
    }

    protected function getMySession() {
        return $this->container->get("session");
    }

    protected function getMyToken() {
        return $this->get("security.context")->getToken();
    }

    protected function getMyUser() {
        return $this->getMyToken()->getUser();
    }

    protected function getMyUserGroupRole() {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository("BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole")
                        ->find($this->getMyUserGroupRoleId());
    }

    protected function checkCSRFProtection() {
        $request = $this->getRequest();
        $uri = $request->getRequestUri();
        $csrfProvider = $this->get('form.csrf_provider');
        if (strstr($uri, "?")) {
            preg_match("/(.*)\?/", $uri, $matches);
            $uri = $matches[1];
        }

        $form = $request->get("form");
        if (!$csrfProvider->isCsrfTokenValid($uri, $form["_token"])) {
            throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('Le jeton CSRF est invalide');
        }

        return true;
    }

    protected function deleteMyEntityFromId(Request $request, $id) {
        try {
            // user can delete this object? => CSRF control
            $this->checkCSRFProtection();

            // get original object from db
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($this->getEntityURI())
                    ->findOneBy(array("id" => $id));

            if (!is_null($entity)) {
                // finalize object and persist it to db
                $em->remove($entity);
                $em->flush();
                $this->successMessage("L'objet a bien été supprimé.");
            } else {
                $this->errorMessage("Impossible de trouver l'objet !");
            }
        } catch (\Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
        } catch (\Exception $e) {
            $this->errorMessage("L'objet ne peut pas être supprimé pour la raison suivante : " . $e->getMessage());
        }

        // exit
        return $this->redirect($this->generateUrl($this->getRoute('index')));
    }

}
