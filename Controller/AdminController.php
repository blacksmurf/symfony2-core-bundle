<?php

namespace BlackSmurf\Symfony2CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Security controller.
 *
 * @Route("/_admin")
 */
class AdminController extends Controller {

    /**
     *
     * @Route("/", name="admin")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($name) {
        return $this->render('Symfony2CoreBundle::index.html.twig', array('name' => $name));
    }

}
