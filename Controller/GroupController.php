<?php

namespace BlackSmurf\Symfony2CoreBundle\Controller;

// SYMFONY //
use Symfony\Component\HttpFoundation\Request;
// ANNOTATIONS //
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

// PROJECT //
use BlackSmurf\Symfony2CoreBundle\Entity\GroupRole;
use BlackSmurf\Symfony2CoreBundle\Form\GroupRoleType;

/**
 * Client controller.
 *
 * @Route("/group")
 */
class GroupController extends EntityController {

    ////////////////////////////////////////////////////////////////////////////
    // MUST BE DEFINED /////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    protected $routes = array(
        "index" => "group_index",
        "new" => "group_new",
        "create" => "group_create",
        "edit" => "group_edit",
        "update" => "group_update",
        "delete" => "group_delete"
    );

    /**
     * Return string object URI
     *
     * @return string
     */
    protected function getEntityURI() {
        return "BlackSmurfBusinessBundle:GroupRole";
    }

    /**
     * Return new object
     *
     * @return GroupRole
     */
    protected function getNewEntity() {
        return new GroupRole();
    }

    /**
     * Return Form's object
     *
     * @return GroupRoleType
     */
    protected function getNewEntityFormType() {
        return new GroupRoleType();
    }

    ////////////////////////////////////////////////////////////////////////////
    // LISTING /////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Lists all User entities.
     *
     * @Route("/", name="group_index")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction() {

    }
}