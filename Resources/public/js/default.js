function doCreate() {
    if (confirm("Etes-vous sur de vouloir valider les données ?")) {
        $("#hidden_form_submit").click();
    }
}

// MAIN ON DOCUMENT LOADED ////////////////////////////////////////////////////
function computeRounded(value) {
    return Math.round(value * 100) / 100;
}

function computeTotalPriceWithHandOff() {
    if ($('#blacksmurf_businessbundle_bill_totalPrice').val() === "") {
        $('#blacksmurf_businessbundle_bill_totalPrice').val("0");
    }

    if ($('#blacksmurf_businessbundle_bill_handOff').val() === "") {
        $('#blacksmurf_businessbundle_bill_handOff').val("0");
    }

    $('#blacksmurf_businessbundle_bill_totalPrice_handOff').html(computeRounded(parseFloat($('#blacksmurf_businessbundle_bill_totalPrice').val().replace(",",".")) * (1 - parseFloat($('#blacksmurf_businessbundle_bill_handOff').val().replace(",",".")) / 100)).toString().replace(".",","));
}

function computeTotalPriceFromBenefits(e) {
    value = parseFloat($("#" + e.attr("id") + "_value").html().replace(",","."));

    oldValue = 0;
    if ($('#blacksmurf_businessbundle_bill_totalPrice').val() != "") {
        oldValue = parseFloat($('#blacksmurf_businessbundle_bill_totalPrice').val().replace(",","."));
    }
    if (e.is(":checked")) {
        $('#blacksmurf_businessbundle_bill_totalPrice').val(computeRounded(oldValue + value).toString().replace(".",","));
    } else {
        $('#blacksmurf_businessbundle_bill_totalPrice').val(computeRounded(oldValue - value).toString().replace(".",","));
    }
}


$(function () {

    if ($('div.bill_benefit input[type=checkbox]').size() != 0) {
        $("div.bill_benefit :checkbox").change(function (e) {
            computeTotalPriceFromBenefits($(this));
            computeTotalPriceWithHandOff();
        });

        $('#blacksmurf_businessbundle_bill_handOff').change(function (e) {
            computeTotalPriceWithHandOff();
        });
    }

    if ($('table.dataTable') !== undefined) {
        $('table.dataTable').DataTable({
            "dom": '<"top"f>rt<"bottom"lp><"clear">',
            "order": [[ 0, 'desc' ]],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/380cb78f450/i18n/French.json"
            }
        });

        $('table.dataTable').addClass("table");
    }

    if ($("form.entityForm input") !== undefined) {
        $("form.entityForm input").on("keypress", function (event) {
            if (event.which == 13 && !event.shiftKey) {
                event.preventDefault();
                doCreate();
            }
        });
    }
})
