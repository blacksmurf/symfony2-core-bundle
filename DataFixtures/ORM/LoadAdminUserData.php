<?php

namespace BlackSmurf\Symfony2CoreBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use \Doctrine\Common\Persistence\ObjectManager;
use BlackSmurf\Symfony2CoreBundle\Entity\User;
use BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole;
use BlackSmurf\Symfony2CoreBundle\Entity\GroupRole;

class LoadAdminUserData extends AbstractFixture implements OrderedFixtureInterface {

    /**
     *
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        $user1 = new User();
        $user1->setLogin("admin@admin.fr");
        $user1->setUsername("Administrateur");
        $user1->setPassword("test");

        $groupRole1 = new GroupRole();
        $groupRole1->setName("Super Administrateur");
        $groupRole1->setRole("ROLE_ADMIN");

        $userGroupRole1 = new UserGroupRole();
        $userGroupRole1->setUser($user1);
        $userGroupRole1->setGroupRole($groupRole1);

        $manager->persist($user1);
        $manager->persist($groupRole1);
        $manager->persist($userGroupRole1);
        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder() {
        return 1;
    }

}
